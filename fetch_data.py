from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, DateTime, UniqueConstraint
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import datetime

engine=create_engine('postgresql+psycopg2://qualdo:qualdo123@localhost:5432/qualdo')


Session=sessionmaker(bind=engine)
session=Session(autocommit=True)

Base=declarative_base()

class Tenants(Base):
	__tablename__ = 'tenants'
	__table_args__ = (UniqueConstraint('tenant_name', 'status_code',
									   name='tenant_uc'), {"schema": "qualdo"})
	tenant_id = Column(Integer, primary_key=True)
	tenant_name = Column(String, nullable=False)
	status_code = Column(Integer, nullable=False, default=1)
	modified_time = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
	created_time = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

	def __iter__(self):
		return self.__dict__.iteritems()

	def as_dict(self):
		return {c.name: getattr(self, c.name) for c in self.__table__.columns}

def get_tenant_details_from_db(tenant_id):
	try:
		result = session.query(Tenants).filter(Tenants.tenant_id == tenant_id).with_entities(Tenants.tenant_name)
		return result

	except Exception as e:
		print(e)
		return None


def update_tenant_details(tenant_id, data):
	try:
		result = session.query(Tenants).filter(Tenants.tenant_id == tenant_id).update({Tenants.tenant_name: data["tenant_name"]})
		return result

	except Exception as e:
		print(e)
		return None

def delete_tenant_details(tenant_id):
	try:
		result = session.query(Tenants).filter(Tenants.tenant_id == tenant_id).delete()
		return result

	except Exception as e:
		print(e)
		return None

def add_tenant(data):
	try:
		result = {"code": 200}
		row = Tenants(**data)
		session.add(row)
		session.flush()
		return result

	except Exception as e:
		print(e)
		result = {"code": 401}
		return result


def get_tenants(tenant_id):
	tenants = get_tenant_details_from_db(tenant_id)
	if tenants:
		final_result = [dict(zip(x.keys(), x)) for x in tenants]
		return {"code": 200, "tenants": final_result}

