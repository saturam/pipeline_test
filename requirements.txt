Flask==1.1.1
SQLAlchemy==1.3.23
pytest~=6.2.5
requests==2.22.0
mock==3.0.5
psycopg2-binary==2.8.6