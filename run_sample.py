from flask import jsonify, request
from fetch_data import get_tenants
from create_app import app


@app.route('/tenants', methods=['POST'])
def get_details():
	tenant_id = request.args.get("tenant_id", None)
	print(tenant_id)
	if tenant_id:
		details = get_tenants(tenant_id)
		return jsonify(details)
	else:
		details = dict(code=401, message="Invalid Tenant id")
		return jsonify(details)

app.run(host="localhost", port=3005)