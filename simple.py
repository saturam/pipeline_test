def add(number1, number2):
    try:
        total = number1 + number2
        print("total",total)
        return total
    except Exception as e:
        return 0


def validate_number(input_number):
    result = {"is_valid": True, "message": "Valid number"}
    if type(input_number) != int:
        result["is_valid"] = False
        result["message"] = "Invalid number"
    return result

def custom_function(input_list):
    result = {"code": 200, "message": "Successfully done", "total": 0}
    if type(input_list) != list:
        result["code"] = 401
        result["message"] = "Unable to do action"
        return result

    total = 0
    for input_number in input_list:
        result = validate_number(input_number)
        if result["is_valid"]:
            total = total + input_number
        else:
            continue

    result["total"] = total
    return result
