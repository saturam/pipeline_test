import mock
from fetch_data import *
import pytest


@mock.patch('fetch_data.session')
def test_get_tenant_details_from_db(mock_session):
    # Case 1 - Valid
    mock_session.query.return_value.filter.return_value.with_entities.side_effect = [1, Exception]
    result = get_tenant_details_from_db(1)
    assert result == 1

    # Case 2: Invalid - Exception
    result = get_tenant_details_from_db("")
    assert result == None


@mock.patch('fetch_data.session')
def test_update_tenant_details(mock_session):
    # Case 1 - Valid
    mock_session.query.return_value.filter.return_value.update.side_effect = [1, Exception]
    result = update_tenant_details(1, {"tenant_name": "team1230"})
    assert result == 1

    # Case 2: Invalid - Exception
    result = update_tenant_details(1, {})
    assert result == None


@mock.patch('fetch_data.session')
def test_delete_tenant_details(mock_session):
    # Case 1 - Valid
    mock_session.query.return_value.filter.return_value.delete.side_effect = [1, Exception]
    result = delete_tenant_details(1)
    assert result == 1

    # Case 2: Invalid - Exception
    result = delete_tenant_details(1)
    assert result == None



