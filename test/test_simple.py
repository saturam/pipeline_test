from simple import *
import mock


def test_validate_number():
    # Case 1: Valid case
    result = validate_number(1)
    assert result["is_valid"] == True
    assert result["message"] == "Valid number"

    # Case 2: Invalid case
    result = validate_number("t")
    assert result["is_valid"] == False
    assert result["message"] == "Invalid number"

@mock.patch('simple.validate_number')
def test_custom_function(mock_validate_number):
    # Case 1 - Valid
    # mock_validate_number.return_value = {"is_valid": True}
    mock_validate_number.side_effect = [{"is_valid": True},{"is_valid": True}, {"is_valid": False}, {"is_valid": True}]
    result = custom_function([1, 3, "9", 7])
    expected_total = 11
    assert result["total"] == expected_total

    # Case 2 - Invalid
    result = custom_function(1)
    expected_total = 0
    assert result["total"] == expected_total




